﻿
using AutoMapper;
using BusinessLogic.Entities;
using BusinessLogic.Interfaces;
using DataAccessLayer;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class ChatServices : IServicesChatBL, IDisposable
    {
        private IUnitOfWork _dB;
        private readonly IMapper _mapper;
        public ChatServices(IMapper mapper)
        {
            _mapper = mapper;
            _dB = new UnitOfWork();
        }
        public async Task CreateAsync(ChatCreateBL chat)
        {
            await _dB.Chats.CreateAsync(chat.idUser1,chat.idUser2);
            await _dB.SaveAsync();
        }
        public async Task DeleteAsync(int id)
        {
            await _dB.Chats.DeleteAsync(id);
            await _dB.SaveAsync();
        }

        public async Task<IEnumerable<ChatBL>> ReadAllAsync()
        {
            var items = await _dB.Chats.ReadAllAsync();
            var result = _mapper.Map<IEnumerable<ChatBL>>(items);

            return result;
        }
        public async Task<ChatBL> ReadByIdAsync(int id)
        {
            var items = await _dB.Chats.ReadByIdAsync(id);
            var result = _mapper.Map<ChatBL>(items);
            return result;
        }
        public async Task<ChatBL> ChatBetweenTwoUsersAsync(int id1, int id2)
        {
            return _mapper.Map<ChatBL>(await _dB.Chats.GetChatBetweenTwoUsersAsync(id1, id2));
        }
        public void Dispose()
        {
            _dB.Dispose();
        }
    }
}
