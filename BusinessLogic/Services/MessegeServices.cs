﻿
using AutoMapper;
using BusinessLogic.Entities;
using BusinessLogic.Interfaces;
using DataAccessLayer;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class MessegeServices : IServicesMessegaBL, IDisposable
    {
        private IUnitOfWork _dB;
        private readonly IMapper _mapper;
        public MessegeServices(IMapper mapper)  
        {
            _mapper = mapper;
            _dB = new UnitOfWork();
        }
        public async Task CreateAsync(MessageCreateBL element)
        {
            element.DateTimeMessege = DateTime.Now;
            var item = _mapper.Map<Message>(element);
            await _dB.Messages.CreateAsync(item);
            await _dB.SaveAsync();
        }

        public async Task DeleteAsync(int id)
        {
            await _dB.Messages.DeleteAsync(id);
            await _dB.SaveAsync();
        }

        public async Task<IEnumerable<MessageBL>> ReadAllAsync()
        {
            var items =await _dB.Messages.ReadAllAsync();
            var result = _mapper.Map<IEnumerable<MessageBL>>(items);

            return result;
        }

        public async Task<MessageBL> ReadByIdAsync(int id)
        {
            var items =await _dB.Messages.ReadAsync(id);
            var result = _mapper.Map<MessageBL>(items);
            return result;
        }

        public async Task UpdateAsync(MessageUpdateBl element, int id)
        {
            var toUpdate =await _dB.Messages.ReadAsync(id);

            if (toUpdate != null)
            {
                toUpdate = _mapper.Map<Message>(element);
                await _dB.Messages.UpdateAsync(toUpdate, id);
                await _dB.SaveAsync();
            }
        }
        public async Task AddMassageAsync(MessageCreateBL message)
        {
            var chat =await _dB.Chats.ReadByIdAsync(message.ChatId);
            if (chat != null)
            {
                await _dB.Messages.AddMassageAsync(message.ChatId, _mapper.Map<Message>(message));
                await _dB.SaveAsync();
            }
        }

        public async Task<IEnumerable<MessageBL>> GetMessageGroupAsync(int idChat)
        {
            return _mapper.Map<IEnumerable<MessageBL>>(await _dB.Messages.GetMessageGroupAsync(idChat));
        }

        public async Task<IEnumerable<MessageBL>> Get20MessageGroupAsync(int idChat, int from)
        {
            var fullBd = _mapper.Map<IEnumerable<MessageBL>>(await _dB.Messages.GetMessageGroupAsync(idChat));
            var countMessage = fullBd.Count();
            fullBd = fullBd.OrderBy(o => o.DateTimeMessege).ToList();
            fullBd = fullBd.Reverse();
            if (from+20 < countMessage)
            {
                fullBd = fullBd.Skip(from).Take(20);
            }
            else
            {
                fullBd = fullBd.Skip(from).Take(countMessage - from);
            }
            return fullBd;
        }
        public void Dispose()
        {
            _dB.Dispose();
        }

        public async Task<MessageBL> ReadByContentAsync(MessageCreateBL message)
        {
            return _mapper.Map<MessageBL>(await _dB.Messages.ReadByContentAsync(_mapper.Map<Message>(message)));
        }
    }
}
