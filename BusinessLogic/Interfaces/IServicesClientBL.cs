﻿using BusinessLogic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IServicesClientBL
    {
        Task CreateAsync(ClientCreateBL element);
        Task<ClientBL> ReadByIdAsync(int id);
        Task<ClientBL> ReadByNameAsync(string name);
        Task<IEnumerable<ClientBL>> ReadAllAsync();
        Task DeleteAsync(int id);
        Task UpdateAsync(ClientCreateBL element, int id);
        Task<IEnumerable<ChatBL>> GetChatsByIdUserAsync(int id);
    }
}
