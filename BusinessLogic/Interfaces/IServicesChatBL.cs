﻿using BusinessLogic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IServicesChatBL
    {
        Task CreateAsync(ChatCreateBL chat);
        Task<IEnumerable<ChatBL>> ReadAllAsync();
        Task DeleteAsync(int id);
        Task<ChatBL> ReadByIdAsync(int id);
        Task<ChatBL> ChatBetweenTwoUsersAsync(int id1, int id2);
    }
}
