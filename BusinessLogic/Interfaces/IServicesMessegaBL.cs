﻿using BusinessLogic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IServicesMessegaBL
    {
        Task CreateAsync(MessageCreateBL element);
        Task<MessageBL> ReadByIdAsync(int id);
        Task<IEnumerable<MessageBL>> ReadAllAsync();
        Task DeleteAsync(int id);
        Task UpdateAsync(MessageUpdateBl element, int id);
        Task AddMassageAsync(MessageCreateBL message);
        Task<MessageBL> ReadByContentAsync(MessageCreateBL message);
        Task<IEnumerable<MessageBL>> GetMessageGroupAsync(int idChat);
        Task<IEnumerable<MessageBL>> Get20MessageGroupAsync(int idChat,int from);
    }
}
