﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Entities
{
    public class ChatBL
    {
        public int Id { get; set; }

        public string User1Name { get; set; }
        public int User1Id { get; set; }

        public string User2Name { get; set; }
        public int User2Id { get; set; }
    }
}
