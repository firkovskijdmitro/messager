﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Entities
{
    public class MessageBL
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime DateTimeMessege { get; set; }
        public int ChatId { get; set; }
        public string SenderName { get; set; }


    }
}
