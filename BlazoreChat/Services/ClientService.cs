﻿using BlazoreChat.Interfaces;
using CommonModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BlazoreChat.Services
{
    public class ClientService : IClientService
    {
        private readonly HttpClient httpClient;
        public ClientService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<IEnumerable<ClientModel>> GetClients()
        {
                return await httpClient.GetFromJsonAsync<ClientModel[]>("api/Client/GetClients");
        }
        public async Task<ClientModel> GetClientOrCreate(string name)
        {
            try{
                var client = await httpClient.GetFromJsonAsync<ClientModel>("api/Client/GetClientByName_" + name);
                return client;
            }
            catch(Exception e )
            {
                if (e.Message == "Response status code does not indicate success: 404 (Not Found).")
                {
                    using var response = await httpClient.PostAsJsonAsync("api/Client/PostClient", new ClientModel() { Name = name });
                    var client = await httpClient.GetFromJsonAsync<ClientModel>("api/Client/GetClientByName_" + name);
                    return client;
                }
                return null;
            }
        }
        public async Task<IEnumerable<ChatModel>> GetChatsByUserId(int useriId)
        {
            return await httpClient.GetFromJsonAsync<ChatModel[]>($"api/Client/GetChatsById_{useriId}");
        }

        public async Task<ClientModel> GetClientById(int id)
        {
            return await httpClient.GetFromJsonAsync<ClientModel>($"api/Client/GetClientById_{id}");
        }
    }
}
