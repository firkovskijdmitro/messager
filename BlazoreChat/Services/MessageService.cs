﻿
using BlazoreChat.Interfaces;
using CommonModels.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BlazoreChat.Services
{
    public class MessageService : IMessageService
    {
        private readonly HttpClient httpClient;
        public MessageService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task DeleteMessage(int id)
        {
            await httpClient.DeleteAsync($"api/Message/DeleteMessageById_{id}");
        }

        public async Task<IEnumerable<MessageModel>> Get20MessageGrop(int idGroup,int from)
        {
            return await httpClient.GetFromJsonAsync<MessageModel[]>($"api/Message/GetTwentyMessageGroup_{idGroup}_{from}");
        }
        public async Task PostMessage(MessageModel message)
        {
            await httpClient.PostAsJsonAsync("api/Message/PostMessage", message);
        }

        public async Task PutMessage(MessageModelUpdate message,int id)
        {
            await httpClient.PutAsJsonAsync($"api/Message/UpdateMessageById_{id}", message);
        }
    }
}
