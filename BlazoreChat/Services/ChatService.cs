﻿using BlazoreChat.Interfaces;
using CommonModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BlazoreChat.Services
{
    public class ChatService : IChatService
    {
        private readonly HttpClient httpClient;
        public ChatService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<ChatModel> GetOrCreateChatBetweenTwoUsers(int id, int id2)
        {
            try
            {
                var chat = await httpClient.GetFromJsonAsync<ChatModel>($"api/ChatContoller/GetChatBetween_{id}_{id2}");
                return chat;
            }
            catch (Exception e)
            {
                if (e.Message == "Response status code does not indicate success: 404 (Not Found).")
                {
                    using var response = await httpClient.PostAsJsonAsync("api/ChatContoller/PostChat", new ChatModelCreate() { idUser1 = id,idUser2 = id2 });
                    var chat = await httpClient.GetFromJsonAsync<ChatModel>($"api/ChatContoller/GetChatBetween_{id}_{id2}");
                    return chat;
                }
                return null;
            }
        }
    }
}
