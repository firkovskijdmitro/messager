﻿using CommonModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazoreChat.Interfaces
{
    public interface IChatService
    {
        Task<ChatModel> GetOrCreateChatBetweenTwoUsers(int id,int id2);
    }
}
