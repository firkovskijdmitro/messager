﻿using CommonModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazoreChat.Interfaces
{
    public interface IClientService
    {
        Task<IEnumerable<ClientModel>> GetClients();
        Task<ClientModel> GetClientById(int Id);
        Task<ClientModel> GetClientOrCreate(string name);
        Task<IEnumerable<ChatModel>> GetChatsByUserId(int userId);
    }
}
