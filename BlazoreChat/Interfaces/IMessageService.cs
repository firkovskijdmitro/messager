﻿using CommonModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazoreChat.Interfaces
{
    public interface IMessageService
    {
        Task<IEnumerable<MessageModel>> Get20MessageGrop(int idGroup,int from);
        Task PostMessage(MessageModel message);
        Task PutMessage(MessageModelUpdate message, int id);
        Task DeleteMessage(int id);

    }
}
