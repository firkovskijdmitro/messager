﻿using CommonModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazoreChat.Models
{
    public class LoginState
    {
        public bool IsLoggedIn { get; set; }
        public ClientModel User { get; set; }
        public event Action OnChange;

        public void SetLogin(bool login, ClientModel user)
        {
            IsLoggedIn = login;
            User = user;
            NotifyStateChanged();
        }

        private void NotifyStateChanged()
        {
            OnChange?.Invoke();
        }
    }
}
