﻿using AutoMapper;
using BusinessLogic.Entities;
using CommonModels.Models;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMessenger.MappingProfiles
{
    public class MessageMappingProfile : Profile
    {
        public MessageMappingProfile()
        {
            CreateMap<MessageBL, MessageModel>();
            CreateMap<MessageModelCreate, MessageCreateBL>();
            CreateMap<MessageModelUpdate, MessageUpdateBl>();

            CreateMap<Message, MessageBL>();
            CreateMap<MessageCreateBL, Message>();
            CreateMap<MessageUpdateBl, Message>();
        }
    }
}
