﻿using AutoMapper;
using BusinessLogic.Entities;
using CommonModels.Models;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMessenger.MappingProfiles
{
    public class ChatProffile : Profile
    {
        public ChatProffile()
        {
            CreateMap<ChatBL, ChatModel>();
            CreateMap<ChatModelCreate, ChatCreateBL>();

            CreateMap<Chat, ChatBL>();
            CreateMap<ChatCreateBL, Chat>();
        }
    }
}
