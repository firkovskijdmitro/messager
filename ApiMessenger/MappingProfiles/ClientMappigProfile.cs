﻿using AutoMapper;
using BusinessLogic.Entities;
using CommonModels.Models;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMessenger.MappingProfiles
{
    public class ClientMappigProfile : Profile
    {
        public ClientMappigProfile()
        {
            CreateMap<ClientBL, ClientModel>();
            CreateMap<ClientModelCreate, ClientCreateBL>();

            CreateMap<Client, ClientBL>();
            CreateMap<ClientCreateBL, Client>();
        }
    }
}
