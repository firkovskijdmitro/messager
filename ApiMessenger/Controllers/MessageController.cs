﻿using ApiMessenger.Hubs;
using AutoMapper;
using BusinessLogic.Entities;
using BusinessLogic.Interfaces;
using CommonModels.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiMessenger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : Controller
    {
        IHubContext<ChatHub> _chatHubContext;
        private readonly IServicesMessegaBL _messageService;
        private readonly IMapper _mapper;
        public MessageController(IMapper mapper, IServicesMessegaBL messageService, IHubContext<ChatHub> chatHubContext)
        {
            _messageService = messageService;
            _mapper = mapper;
            _chatHubContext = chatHubContext;
        }
        [HttpGet]
        [Route("GetAllMessages")]
        public async Task<IActionResult> GetAllAsync()
        {
            try
            {
                List<MessageModel> list = null;
                list = _mapper.Map<List<MessageModel>>(await _messageService.ReadAllAsync());
                if (list == null)
                    return NotFound();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        [HttpGet]
        [Route("GetMessageById_{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var result = _mapper.Map<MessageModel>(await _messageService.ReadByIdAsync(id));
            if (result == null)
                return NotFound();
            return Ok(result);
        }
        [HttpDelete]
        [Route("DeleteMessageById_{id}")]
        public async Task<IActionResult> DeleteMessageAsync(int id)
        {
            var message = await _messageService.ReadByIdAsync(id);
            if (message == null)
                return NotFound();
            await _chatHubContext.Clients.Group(message.ChatId.ToString()).SendAsync("DeleteMessage", message);
            await _messageService.DeleteAsync(id);
            return NoContent();
        }
        [HttpPut]
        [Route("UpdateMessageById_{id}")]
        public async Task<IActionResult> UpdateMessageAsync(int id, [FromBody] MessageModelUpdate model)
        {
            var message = await _messageService.ReadByIdAsync(id);
            if (message == null)   
                return NotFound();
            await _messageService.UpdateAsync(_mapper.Map<MessageUpdateBl>(model), id);
            message = await _messageService.ReadByIdAsync(id);
            await _chatHubContext.Clients.Group(message.ChatId.ToString()).SendAsync("UpdateMessage", message);
            return Ok();
        }
        [HttpPost]
        [Route("PostMessage")]
        public async Task<IActionResult> PostMessageAsync([FromBody] MessageModelCreate message)
        {
            if (message == null)
                return BadRequest();
            await _messageService.AddMassageAsync(_mapper.Map<MessageCreateBL>(message));
            var newMessage = await _messageService.ReadByContentAsync(_mapper.Map<MessageCreateBL>(message));
            await _chatHubContext.Clients.Group(message.ChatId.ToString()).SendAsync("GetMessage", newMessage);
            return StatusCode((int)HttpStatusCode.Created);
        }
        [HttpGet]
        [Route("GetMessageGroup_{id}")]
        public async Task<IActionResult> GetMessageGroupAsync(int id)
        {
            var list = _mapper.Map<IEnumerable<MessageModel>>(await _messageService.GetMessageGroupAsync(id));
            return Ok(list);
        }
        [HttpGet]
        [Route("GetTwentyMessageGroup_{id}_{from}")]
        public async Task<IActionResult> Get20MessageGroupAsync(int id,int from)
        {
            var list = _mapper.Map<IEnumerable<MessageModel>>(await _messageService.Get20MessageGroupAsync(id,from));
            return Ok(list);
        }
    }
}
