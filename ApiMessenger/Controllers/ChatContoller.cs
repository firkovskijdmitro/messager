﻿using AutoMapper;
using BusinessLogic.Entities;
using BusinessLogic.Interfaces;
using CommonModels.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiMessenger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatContoller : Controller
    {
        private readonly IServicesChatBL _chatService;
        private readonly IMapper _mapper;
        public ChatContoller(IMapper mapper, IServicesChatBL chatService)
        {
            _chatService = chatService;
            _mapper = mapper;
        }
        [HttpPost]  
        [Route("PostChat")]
        public async Task<IActionResult> PostChatAsync([FromBody] ChatModelCreate chat)
        {
            if (chat.idUser1 <= 0 || chat.idUser2 <= 0)
                return BadRequest();
            await _chatService.CreateAsync(_mapper.Map<ChatCreateBL>(chat));
            return StatusCode((int)HttpStatusCode.Created);
        }
        [HttpGet]
        [Route("GetAllChats")]
        public async Task<IActionResult> GetAllAsync()
        {
            try
            {
                List<ChatModel> list = null;
                list = _mapper.Map<List<ChatModel>>(await _chatService.ReadAllAsync());
                if (list == null)
                    return NotFound();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        [HttpGet]
        [Route("GetChatById_{id}")]
        public async Task<IActionResult> GetByIDAsync(int id)
        {
            var result = _mapper.Map<ChatModel>(await _chatService.ReadByIdAsync(id));
            if (result == null)
                return NotFound();
            return Ok(result);
        }
        [HttpDelete]
        [Route("DeleteChatById_{id}")]
        public async Task<IActionResult> DeleteClientAsync(int id)
        {
            if (await _chatService.ReadByIdAsync(id) == null)
                return NotFound();
            await _chatService.DeleteAsync(id);
            return NoContent();
        }
        [HttpGet]
        [Route("GetChatBetween_{id1}_{id2}")]
        public async Task<IActionResult> GetChatBetweenUsersAsync(int id1,int id2)
        {
            var result = _mapper.Map<ChatModel>(await _chatService.ChatBetweenTwoUsersAsync(id1,id2));
            if (result == null)
                return NotFound();
            return Ok(result);
        }

    }
}
