﻿using AutoMapper;
using BusinessLogic.Entities;
using BusinessLogic.Interfaces;
using CommonModels.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiMessenger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : Controller
    {
        private readonly IServicesClientBL _clientService;
        private readonly IMapper _mapper;
        public ClientController(IMapper mapper, IServicesClientBL clientService)
        {
            _clientService = clientService;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetClients")]
        public async Task<IActionResult> GetAllAsync()
        {
            try
            {
                List<ClientModel> list = null;
                list = _mapper.Map<List<ClientModel>>(await _clientService.ReadAllAsync());
                if (list == null)
                    return NotFound();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }

        [HttpPost]
        [Route("PostClient")]
        public async Task<IActionResult> PostClientAsync([FromBody] ClientModelCreate model)
        {
            if (model == null)
                return BadRequest();
            var result = _mapper.Map<ClientCreateBL>(model);
            await _clientService.CreateAsync(result);
            return StatusCode((int)HttpStatusCode.Created);
        }
        [HttpGet]
        [Route("GetClientById_{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var result = _mapper.Map<ClientModel>( await _clientService.ReadByIdAsync(id));
            if (result == null)
                return NotFound();
            return Ok(result);
        }
        [HttpDelete]
        [Route("DeleteClientById_{id}")]
        public async Task<IActionResult> DeleteClientAsync(int id)
        {
            if (await _clientService.ReadByIdAsync(id) == null)
                return NotFound();
            await _clientService.DeleteAsync(id);
            return NoContent();
        }
        [HttpPut]
        [Route("PutClientById_{id}")]
        public async Task<IActionResult> UpdateClientAsync(int id, [FromBody] ClientModelCreate model)
        {
            if (await _clientService.ReadByIdAsync(id) == null)
                return NotFound();
            await _clientService.UpdateAsync(_mapper.Map<ClientCreateBL>(model), id);
            return Ok();
        }
        [HttpGet]
        [Route("GetChatsById_{clientId}")]
        public async Task<IActionResult> GetChatsAsync(int clientId)
        {
            var chats = _mapper.Map<IEnumerable<ChatModel>>(await _clientService.GetChatsByIdUserAsync(clientId));
            if (chats == null)
                return NotFound();
            return (Ok(chats));
        }
        [HttpGet]
        [Route("GetClientByName_{name}")]
        public async Task<IActionResult> GetByNameAsync(string name)
        {
            var result = _mapper.Map<ClientModel>(await _clientService.ReadByNameAsync(name));
            if (result == null)
                return NotFound();
            return Ok(result);
        }
    }
}
