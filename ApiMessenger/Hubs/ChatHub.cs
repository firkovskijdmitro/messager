﻿using CommonModels.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMessenger.Hubs
{
    public class ChatHub : Hub
    {
        public async Task JoinChat(string nameChat)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, nameChat);
        }
        public async Task AddMessageToChat(MessageModel message)
        {
            await Clients.Group(message.ChatId.ToString()).SendAsync("GetMessage", message);
        }
        public async Task DeleteMessageFromChat(MessageModel deleteMessage, string nameChat)
        {
            await Clients.Group(nameChat).SendAsync("DeleteMessage", deleteMessage);
        }
        public async Task UpdateMessageFromChat(MessageModel updateMessage, string nameChat)
        {
            await Clients.Group(nameChat).SendAsync("UpdateMessage", updateMessage);
        }
    }
}
