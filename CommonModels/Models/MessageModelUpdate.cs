﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommonModels.Models
{
    public class MessageModelUpdate
    {
        public string Content { get; set; }
    }
}
