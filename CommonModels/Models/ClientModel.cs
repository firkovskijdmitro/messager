﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonModels.Models
{
    public class ClientModel
    {
        public int id { get; set; }
        public string Name { get; set; }
    }
}
