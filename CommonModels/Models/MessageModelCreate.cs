﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonModels.Models
{
    public class MessageModelCreate
    {
        public string Content { get; set; }
        public DateTime DateTimeMessege { get; set; }
        public int ChatId { get; set; }
        public string SenderName { get; set; }
    }
}
