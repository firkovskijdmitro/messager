﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommonModels.Models
{
    public class ChatModel
    {
        public int Id { get; set; }

        public string User1Name { get; set; }
        public int User1Id { get; set; }

        public string User2Name { get; set; }
        public int User2Id { get; set; }
    }
}
