﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Message
    {
        public int Id { get; set; }   
        public string Content { get; set; }
        public DateTime DateTimeMessege { get; set; }

        public int ChatId { get; set; }
        public Chat Chat { get; set; }
        public string SenderName { get; set; }
    }
}
