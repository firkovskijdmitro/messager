﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Chat
    {
        public int Id { get; set; }

        public string User1Name { get; set; }
        public int User1Id { get; set; }
        public Client User1 { get; set; }

        public string User2Name { get; set; }
        public int User2Id { get; set; }
        public Client User2 { get; set; }

        public List<Message> Messages { get; set; } = new List<Message>();
    }

}
