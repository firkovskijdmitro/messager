﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class GroupChat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AdminId { get; set; }
        public Client Admin { get; set; }
        public List<Message> Messages { get; set; } = new List<Message>();
        public List<Client> Users { get; set; } = new List<Client>();

    }
}
