﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Chat> MainUserFriends { get; set; }
        public virtual ICollection<Chat> UsersFriendships { get; set; }
    }
}
