﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IRepositoryClient
    {
        Task<IEnumerable<Client>> ReadAllAsync();
        Task<Client> ReadByIdAsync(int id);
        Task<Client> ReadByNameAsync(string name);
        Task CreateAsync(Client item);
        Task DeleteAsync(int id);
        Task UpdateAsync(Client item, int id);
        Task<IEnumerable<Chat>> GetChatsByIdUserAsync(int id);
    }
}
