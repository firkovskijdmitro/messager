﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepositoryClient Clients { get; }
        IRepositoryChat Chats { get; }
        IRepositoryMessage Messages { get; }
        Task SaveAsync();
    }
}
