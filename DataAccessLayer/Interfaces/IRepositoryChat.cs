﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IRepositoryChat
    {
        Task<IEnumerable<Chat>> ReadAllAsync();
        Task<Chat> ReadByIdAsync(int id);
        Task CreateAsync(int idUser1, int idUser2);
        Task DeleteAsync(int id);
        Task<Chat> GetChatBetweenTwoUsersAsync(int id, int id2);
    }
}
