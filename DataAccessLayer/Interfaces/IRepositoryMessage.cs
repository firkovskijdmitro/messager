﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IRepositoryMessage
    {
        Task<IEnumerable<Message>> ReadAllAsync();
        Task<Message> ReadAsync(int id);
        Task<Message> ReadByContentAsync(Message message);
        Task CreateAsync(Message item);
        Task UpdateAsync(Message item, int id);
        Task DeleteAsync(int id);
        Task AddMassageAsync(int idChat, Message message);
        Task<IEnumerable<Message>> GetMessageGroupAsync(int idChat);
    }
}
