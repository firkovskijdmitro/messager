﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.EF;
using DataAccessLayer.Repositories;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class UnitOfWork: IUnitOfWork
    {
        private Context _dataBase { get; }
        private IRepositoryClient _clientRepository;
        private IRepositoryMessage _messageRepository;
        private IRepositoryChat _chatRepository;

        public UnitOfWork()
        {
            _dataBase = new Context();
        }

        public IRepositoryClient Clients
        {
            get
            {
                if (_clientRepository == null)
                    _clientRepository = new ClientRepository(_dataBase);
                return _clientRepository;
            }
        }
        public IRepositoryMessage Messages
        {
            get
            {
                if (_messageRepository == null)
                    _messageRepository = new MessegeRepository(_dataBase);
                return _messageRepository;
            }
        }
        public IRepositoryChat Chats
        {
            get
            {
                if (_chatRepository == null)
                    _chatRepository = new ChatRepository(_dataBase);
                return _chatRepository;
            }
        }
        public async Task SaveAsync()
        {
            await _dataBase.SaveChangesAsync();    
        }

        public void Dispose()
        {
            _dataBase.Dispose();
        }
    }
}
