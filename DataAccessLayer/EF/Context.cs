﻿ using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DataAccessLayer.Entities;

namespace DataAccessLayer.EF
{
    public class Context : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Chat> Chats { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=tcp:messagerdatabase.database.windows.net,1433;Initial Catalog=messagerdatabase;Persist Security Info=False;User ID=messagerdatabase;Password=1505ff15@@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
                  }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chat>()
                .HasKey(f => new { f.User1Id, f.User2Id });

            modelBuilder.Entity<Chat>()
                .HasOne(f => f.User1)
                .WithMany(mu => mu.MainUserFriends)
                .HasForeignKey(f => f.User1Id)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Chat>()
                .HasOne(f => f.User2)
                .WithMany(mu => mu.UsersFriendships)
                .HasForeignKey(f => f.User2Id);
            modelBuilder.Entity<Chat>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();
        }
    }
}

