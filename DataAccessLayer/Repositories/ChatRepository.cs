﻿using DataAccessLayer.EF;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class ChatRepository : IRepositoryChat
    {
        private readonly Context _dB;

        public ChatRepository(Context context)
        {
            _dB = context;
        }
        public async Task CreateAsync(int idUser1,int idUser2)
        {
            Client user1 = _dB.Clients.Find(idUser1);
            Client user2 = _dB.Clients.Find(idUser2);
            if(user1 != null && user2 != null)
            {
                var chat = new Chat()
                {
                    User1 = user1,
                    User1Id = user1.Id,
                    User1Name = user1.Name,
                    User2 = user2,
                    User2Id = user2.Id,
                    User2Name = user2.Name
                };
                await _dB.Chats.AddAsync(chat);
            }
        }
        public async Task DeleteAsync(int id)
        {
            Chat chat = await _dB.Chats.FirstOrDefaultAsync(p => p.Id == id); ;
            if (chat != null)
               _dB.Chats.Remove(chat);
        }

        public async Task<Chat> ReadByIdAsync(int id)
        {
            return await _dB.Chats.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IEnumerable<Chat>> ReadAllAsync()
        {
            return await _dB.Chats.ToListAsync();
        }
        public async Task<Chat> GetChatBetweenTwoUsersAsync(int id1, int id2)
        {
            return await _dB.Chats.FirstOrDefaultAsync(ch => (ch.User1Id == id1 && ch.User2Id == id2 ) || (ch.User1Id == id2 && ch.User2Id == id1));
        }
    }
}
    