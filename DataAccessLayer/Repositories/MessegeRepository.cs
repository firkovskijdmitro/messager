﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.EF;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Repositories
{
    public class MessegeRepository : IRepositoryMessage
    {
        private readonly Context _dB;

        public MessegeRepository(Context context)
        {
            _dB = context;
        }

        public async Task<IEnumerable<Message>> ReadAllAsync()
        {
            return await _dB.Messages.ToListAsync();
        }
        public async Task<Message> ReadAsync(int id)
        {
            return await _dB.Messages.FindAsync(id);
        }
        public async Task CreateAsync(Message item)
        {
           await _dB.Messages.AddAsync(item);
        }
        public async Task UpdateAsync(Message item,int id)
        {
            var previousMessage =await _dB.Messages.FindAsync(id);
            previousMessage.Content = item.Content;
        }
        public async Task DeleteAsync(int id)  
        {
            Message item = await _dB.Messages.FindAsync(id);
            if (item != null)
                _dB.Messages.Remove(item);
        }
        public async Task AddMassageAsync(int idChat, Message message)
        {
            Chat chat =await _dB.Chats.FirstOrDefaultAsync(с => с.Id == idChat);
            if (chat != null)
            {
                message.Chat = chat;
                message.ChatId = idChat;
                await _dB.Messages.AddAsync(message);
            }
        }
        public async Task<IEnumerable<Message>> GetMessageGroupAsync(int idChat)
        {
            return await _dB.Messages.Where(с => с.ChatId == idChat).ToListAsync();
        }

        public async Task<Message> ReadByContentAsync(Message message)
        {
            return await _dB.Messages.FirstOrDefaultAsync(m => m.Content == message.Content && m.DateTimeMessege == message.DateTimeMessege && m.ChatId == message.ChatId);
        }
    }
}
