﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.EF;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class ClientRepository : IRepositoryClient
    {
        private readonly Context _dB;

        public ClientRepository(Context context)
        {
            _dB = context;
        }

        public async Task<IEnumerable<Client>> ReadAllAsync()
        {
            return await _dB.Clients.ToListAsync();
        }
        public async Task<Client> ReadByIdAsync(int id)
        {
            return await _dB.Clients.FindAsync(id);
        }
        public async Task CreateAsync(Client client)
        {
            await _dB.Clients.AddAsync(client);
        }
        public async Task UpdateAsync(Client client, int id)
        {
            var previousClient=await _dB.Clients.FindAsync(id);

            if (previousClient != null)
            {
                _dB.Clients.Remove(previousClient);
                Client newClient = new Client()
                {
                    Name = client.Name,
                };

                await _dB.Clients.AddAsync(newClient);
            }
        }
        public async Task DeleteAsync(int id)
        {
            Client client =await _dB.Clients.FindAsync(id);
            if (client != null)
                _dB.Clients.Remove(client);
        }
        public async Task<IEnumerable<Chat>> GetChatsByIdUserAsync(int id)
        {
            var chats = await _dB.Chats.Where(p => p.User1Id == id || p.User2Id == id).ToListAsync();
            return chats;
        }

        public async Task<Client> ReadByNameAsync(string name)
        {
            return await _dB.Clients.FirstOrDefaultAsync(client => client.Name == name);
        }
    }
}
